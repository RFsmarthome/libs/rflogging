#ifndef _RF_LOGGING_H
#define _RF_LOGGING_H

#include <uuid/common.h>
#include <uuid/log.h>

extern void loggingInit(const String &hostname);
extern void loggingLoop();

#endif
