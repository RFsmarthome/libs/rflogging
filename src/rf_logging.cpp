#include <uuid/common.h>
#include <uuid/log.h>
#ifndef CONSOLE_LOGGING
#include <uuid/syslog.h>
#endif

#include "rf_logging.h"

#ifndef CONSOLE_LOGGING
static uuid::syslog::SyslogService syslog;
static IPAddress syslogDestination(192, 168, 178, 39);
#else
class SerialLogHandler : public uuid::log::Handler {
public:
    SerialLogHandler() = default;
    ~SerialLogHandler() {
        uuid::log::Logger::unregister_handler(this);
    }

    void start() {
        uuid::log::Logger::register_handler(this, uuid::log::Level::ALL);
    };

    void operator<<(std::shared_ptr<uuid::log::Message> message) {
        char temp[100] = { 0 };
		int ret = snprintf_P(temp, sizeof(temp), PSTR("%s %c [%S] %s\r\n"),
			uuid::log::format_timestamp_ms(message->uptime_ms).c_str(),
			uuid::log::format_level_char(message->level),
			message->name, message->text.c_str());
		if (ret > 0) {
			Serial.print(temp);
		}
    }
};
static SerialLogHandler g_log_handler;
#endif

void loggingInit(const String &hostname)
{
    #ifndef CONSOLE_LOGGING
    syslog.start();
    syslog.hostname(hostname.c_str());
    syslog.log_level(uuid::log::ALL);
    syslog.mark_interval(3600);
    syslog.destination(syslogDestination);
    #else
    g_log_handler.start();
    #endif
}

void loggingLoop()
{
    uuid::loop();
    #ifndef CONSOLE_LOGGING
    syslog.loop();
    #endif
}
